# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from dateutil.relativedelta import relativedelta
from string import Template
from email.header import Header
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.utils import formatdate, make_msgid
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Bool, Not, Eval, Equal, Or, In
from trytond.tools import get_smtp_server
from trytond.transaction import Transaction
from trytond.config import CONFIG
from trytond.pool import Pool
from trytond.wizard import Wizard


STATES = {'readonly': Bool(In(Eval('state'), ['active', 'closed']))}
REMINDER_RELATIVEDELTA = {
    '-15m': relativedelta(minutes=-15),
    '-30m': relativedelta(minutes=-30),
    '-1h': relativedelta(hours=-1),
    '-1d': relativedelta(days=-1),
    '+1h': relativedelta(hours=+1),
    '+1d': relativedelta(days=+1),
    '+1w': relativedelta(weeks=+1),
    '+1M': relativedelta(months=+1),
    '+1Y': relativedelta(years=+1),
    }
REMINDER_SELECTION = [
    ('custom', 'Custom'),
    ('-15m', '15 minutes before'),
    ('-30m', '30 minutes before'),
    ('-1h', 'One hour before'),
    ('-1d', 'One day before'),
    ('+1h', 'One hour later'),
    ('+1d', 'One day later'),
    ('+1w', 'One week later'),
    ('+1M', 'One month later'),
    ('+1Y', 'One year later'),
    ]


class ReminderEmailTemplate(ModelSQL, ModelView):
    'Reminder Email Template'
    _name = 'reminder.email.template'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy',
        help='The name of the email template.')
    message = fields.Text('Message', translate=True, loading='lazy',
        help='The body text of the email template.\n\n'
        'The following placeholders can be used in the message.\n\n'
        'General Placeholders:\n'
        ' * $from_user_name: The name of the execution user for scheduler '
            "action 'Process Reminder'. This is the internal user who sends "
            'reminder emails.\n'
        ' * $to_user_name: The name of the user which receives the reminder.\n'
        'Technical Placeholders:\n'
        ' * $to_user_login: The login of the reminder receiving user.\n\n'
        ' * $database_name: The name of the actual database.\n'
        ' * $reminder_id: The internal id of the reminder.\n'
        ' * $host: The host name the Tryton server.\n'
        ' * $port: The port the Tryton server.'
        )

ReminderEmailTemplate()


class Reminder(ModelSQL, ModelView):
    'Reminder'
    _name = 'reminder.reminder'
    _description = __doc__
    _rec_name = 'summary'

    summary = fields.Char('Summary')
    user = fields.Many2One('res.user', 'User', required=True, states=STATES,
        depends=['state'])
    presetting = fields.Selection(REMINDER_SELECTION, 'Presetting', required=True,
        sort=False, states=STATES, on_change=[
            'presetting', 'time', 'user', 'reference', 'old_time',
        ], depends=['state', 'reference', 'old_time'],
        help='Presettings for the reminder. Choose a relative '
        "time delta option. The reminders 'Time' field is calculated from "
        "the value given in the 'Time' field or from the actual point "
        'in time.\n'
        "Choose option 'Custom' to manual set-up the time to remind.")
    time = fields.DateTime('Time', states={
            'readonly': Or(
                Not(Bool(Equal(Eval('presetting'), 'custom'))),
                Bool(In(Eval('state'), ['active', 'closed']))),
            }, depends=['presetting', 'state'], required=True,
        help='The latest point in time for the reminder activation.\n'
        'The exact time of activation is depending on the interval setting of '
        "the scheduler action 'Process Reminders'.")
    old_time = fields.Function(fields.DateTime('Old Time',
        states={
            'invisible': True,
            }), 'get_old_time')
    email = fields.Boolean('Email', states=STATES, depends=['state'],
        help='When checked, the reminder will be send as an email on '
        'activation.\n'
        'The user in the reminder must have a valid email address set '
        'in the user preferences.\n')
    email_template = fields.Many2One('reminder.email.template',
        'Email Template', states={
            'invisible': Not(Bool(Eval('email'))),
            'required': Bool(Eval('email')),
            'readonly': Bool(In(Eval('state'), ['active', 'closed'])),
            }, depends=['email', 'state'])
    state = fields.Selection([
            ('active', 'Active'),
            ('planned', 'Planned'),
            ('closed', 'Closed'),
            ], 'State', readonly=True, sort=False, states=STATES)
    state_icon = fields.Function(
        fields.Char('State Icon', on_change_with=['state'], depends=['state']),
        'get_state_icon')
    reference = fields.Reference('Reference', selection='links_get',
        states=STATES, depends=['state'])

    def __init__(self):
        super(Reminder, self).__init__()
        self._rpc.update({
                'set_process_reminders_planned': True,
                'set_process_reminders_active': True,
                'set_process_reminders_closed': True,
        })
        self._error_messages.update({
                'missing_from_email': 'No email address defined for '
                    'scheduler user %s (%s)!\n'
                    'Please define an email address in the user preferences.\n'
                    'Hint: Search the scheduler user with search clause:\n'
                    'Active:False',
                'missing_to_email': 'No email address defined for '
                    'reminder user %s (%s)!\n'
                    'Please define an email address in the user preferences.',
                'smtp_error': "SMTP Error: Can not send email.\n\n"
                    "Details:\n\n%s"
                })
        self._order.insert(0, ('state', 'ASC'))
        self._order.insert(1, ('time', 'DESC'))

    def default_state(self):
        return 'planned'

    def default_user(self):
        return int(Transaction().user)

    def default_email(self):
        return True

    def default_email_template(self):
        config_obj = Pool().get('reminder.configuration')
        config = config_obj.browse(config_obj.get_singleton_id())
        return config.reminder_email_template.id

    def default_presetting(self):
        return 'custom'

    def default_old_time(self):
        return (Transaction().context.get('reminder_base_time') or
            datetime.datetime.now())

    def default_reference(self):
        res = False
        context = Transaction().context
        if ('reminder_reference_model' in context and
                'reminder_reference_id' in context and
                context['reminder_reference_id'] > 0):
            res = (context['reminder_reference_model'] + ',' +
                str(context['reminder_reference_id']))
        return res

    def on_change_presetting(self, vals):
        res = {}
        time = datetime.datetime.now()
        if Transaction().context.get('reminder_base_time'):
            time = Transaction().context['reminder_base_time']
        elif vals.get('reference'):
            reference_model, ref_id = vals['reference'].split(',')
            reference_obj = Pool().get(reference_model)
            if hasattr(reference_obj, 'get_reminder_base_time'):
                time = reference_obj.get_reminder_base_time(int(ref_id))
        elif vals.get('time'):
            time = vals['time'] + (vals['old_time'] - vals['time'])
        if vals.get('presetting'):
            if vals['presetting'] != 'custom':
                res['time'] = (
                    time + REMINDER_RELATIVEDELTA[vals['presetting']])
            else:
                res['time'] = vals['old_time']
        return res

    def on_change_with_state_icon(self, values):
        return self._get_state_icon(values.get('state', ''))

    def links_get(self):
        reminder_link_obj = Pool().get('reminder.link')
        ids = reminder_link_obj.search([])
        reminder_links = reminder_link_obj.browse(ids)
        return [(x.model, x.name) for x in reminder_links]

    def get_state_icon(self, ids, name):
        res = {}
        for reminder in self.browse(ids):
            res[reminder.id] = self._get_state_icon(reminder.state)
        return res

    def _get_state_icon(self, state):
        res = ''
        if state == 'active':
            res = 'tryton-state-green'
        elif state == 'planned':
            res = 'tryton-state-orange'
        elif state == 'closed':
            res = 'tryton-state-grey'
        return res

    def get_old_time(self, ids, name):
        reminders = self.browse(ids)
        res = {}
        for reminder in reminders:
            res[reminder.id] = reminder.time
        return res

    def process_reminders(self):
        cron_obj = Pool().get('ir.cron')

        scheduler_id, = cron_obj.search([
                ('name','=','Process Reminders'),
                ('model', '=', 'reminder.reminder'),
                ('function', '=', 'process_reminders'),
                ], limit=1)
        scheduler = cron_obj.browse(scheduler_id)
        args = [
            ('time', '<=', scheduler.next_call),
            ('state', '=', 'planned'),
            ]
        reminders_to_process_values = self.search_read(args,
            fields_names=['email', 'state'])
        email_ids = []
        reminders_to_process_ids = []
        for value in reminders_to_process_values:
            reminders_to_process_ids.append(value['id'])
            if value.get('email') and value.get('state') != 'closed':
                email_ids.append(value['id'])
        self.process_email_reminders(email_ids)
        self.set_process_reminders_active(reminders_to_process_ids)

    def set_process_reminders_planned(self, ids):
        self.write(ids, {'state': 'planned'})

    def set_process_reminders_active(self, ids):
        self.write(ids, {'state': 'active'})

    def set_process_reminders_closed(self, ids):
        self.write(ids, {'state': 'closed'})

    def process_email_reminders(self, ids):
        user_obj = Pool().get('res.user')
        config_obj = Pool().get('reminder.configuration')
        config = config_obj.browse(config_obj.get_singleton_id())

        scheduler_user = user_obj.browse(Transaction().user)
        if not scheduler_user.email:
            self.raise_user_error('missing_from_email',
                error_args=(scheduler_user.name, scheduler_user.login))
        for reminder in self.browse(ids):
            if not reminder.user.email:
                self.raise_user_error('missing_to_email',
                    error_args=(reminder.user.name, reminder.user.login))
            language_code = (reminder.user.language.code if
                reminder.user.language else 'en_US')
            with Transaction().set_context(language=language_code):
                message = reminder.email_template.message
                message = self._process_message(message, reminder,
                    scheduler_user)

            msg = MIMEMultipart()
            msg['Subject'] = Header(reminder.summary or 'No subject', 'utf-8')

            if config.reminder_from_email_prefix:
                msg['From'] = '%s <%s>' % (config.reminder_from_email_prefix,
                    scheduler_user.email)
            else:
                msg['From'] = scheduler_user.email

            msg['To'] = '%s <%s>' % (reminder.user.name, reminder.user.email)
            msg['Date'] = formatdate()
            msg['Message-ID'] = make_msgid()
            msg.attach(MIMEText(message.strip(), _charset='utf-8'))
            smtp_server = get_smtp_server()
            res = smtp_server.sendmail(
                scheduler_user.email,
                reminder.user.email,
                msg.as_string())
            if res:
                self.raise_user_error('smtp_error', error_args=(res,))

        return True

    def _process_message(self, message, reminder, user):
        message = Template(message)
        from_user_name = user.name
        to_user_name = reminder.user.name
        to_user_login = reminder.user.login
        cursor = Transaction().cursor
        database = cursor.database_name
        host, port = CONFIG.get('jsonrpc')[0]
        reminder_id = reminder.id
        message = message.substitute(
            from_user_name=from_user_name,
            to_user_name=to_user_name,
            to_user_login=to_user_login,
            database=database,
            host=host,
            port=port,
            reminder_id=reminder_id
            )
        return message

Reminder()


class ReminderLink(ModelSQL, ModelView):
    "Reminder link"
    _name = 'reminder.link'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True)
    model = fields.Selection('models_get', 'Model', required=True)
    priority = fields.Integer('Priority')

    def __init__(self):
        super(ReminderLink, self).__init__()
        self._order.insert(0, ('priority', 'ASC'))

    def default_priority(self):
        return 5

    def models_get(self):
        model_obj = Pool().get('ir.model')
        model_ids = model_obj.search([])
        res = []
        for model in model_obj.browse(model_ids):
            res.append((model.model, model.name))
        return res

ReminderLink()


class PlanReminderAsk(ModelView):
    'Plan Reminder Ask'
    _name = 'reminder.plan_reminder.ask'
    _description = __doc__

    presetting = fields.Selection(REMINDER_SELECTION, 'Presetting',
        required=True, sort=False, on_change=[
            'presetting', 'time',
        ],
        help='Presettings for the reminder. Choose a relative '
        "time delta option. The reminders 'Time' field is calculated from "
        "the value given in the 'Time' field or from the actual point "
        'in time.\n'
        "Choose option 'Custom' to manual set-up the time to remind.")
    time = fields.DateTime('Time', states={
            'readonly': Not(Bool(Equal(Eval('presetting'), 'custom'))),
        }, depends=['presetting'],
        help='The latest point in time for the reminder activation.\n'
        'The exact time of activation is depending on the interval setting of '
        "the scheduler action 'Process Reminders'.")
    email = fields.Boolean('Email',
        help='When checked, the reminder will be send as an email on '
        'activation.\n'
        'The user in the reminder needs to setup a valid email address, which '
        'is defined in the user preferences.\n'
        'The needed SMTP connection to send the email is set in the '
        'configuration file of the Tryton server.' )
    email_template = fields.Many2One('reminder.email.template',
        'Email Template', states={
            'invisible': Not(Bool(Eval('email'))),
            'required': Bool(Eval('email')),
            }, depends=['email'],
        help='The email template to use in the reminder email.\n'
        'Email templates are defined in the reminder configuration.')

    def default_email(self):
        return True

    def default_presetting(self):
        return '+1d'

    def on_change_presetting(self, vals):
        res = {}
        date = datetime.datetime.now()
        if vals.get('presetting'):
            if vals['presetting'] != 'custom':
                res['time'] = (
                    date + REMINDER_RELATIVEDELTA[vals['presetting']])
            elif not vals['time']:
                res['time'] = date
        return res

    def default_email_template(self):
        config_obj = Pool().get('reminder.configuration')
        config = config_obj.browse(config_obj.get_singleton_id())
        return config.reminder_email_template.id

PlanReminderAsk()


class PlanReminder(Wizard):
    'Plan Reminder'
    _name = 'reminder.plan_reminder'

    states = {
        'init': {
            'result': {
                'type': 'form',
                'object': 'reminder.plan_reminder.ask',
                'state': [
                    ('end', 'Cancel', 'tryton-cancel'),
                    ('plan', 'Plan', 'tryton-ok', True),
                ],

            },
        },
        'plan': {
            'result': {
                'type': 'action',
                'action': '_plan',
                'state': 'end',
            },
        },
    }

    def _plan(self, data):
        reminder_obj = Pool().get('reminder.reminder')
        vals = self._get_vals_reminder(data)
        reminder_obj.write(data['ids'], vals)

    def _get_vals_reminder(self, data):
        return {
            'presetting': data['form']['presetting'],
            'time': data['form']['time'],
            'email': data['form']['email'],
            'email_template': data['form']['email_template'],
            'state': 'planned',
        }

PlanReminder()
