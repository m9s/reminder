# -*- coding: utf-8 -*-
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Reminder',
    'name_de_DE': 'Wiedervorlage',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Allows to shedule reminders.
    ''',
    'description_de_DE': '''Ermöglicht die Erstellung von Wiedervorlagen.
    ''',
    'depends': [
        'ir',
        'res',
    ],
    'xml': [
        'configuration.xml',
        'reminder.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
